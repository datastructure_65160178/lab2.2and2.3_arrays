public class ArrayDeletionsLab3 {
    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0; 
        }
        
        int k = 1; 
        
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[k] = nums[i];
                k++;
            }
        }
        
        return k;
    }

    public static void main(String[] args) {
        int[] nums = {0,0,1,1,1,2,2,3,3,4};
        
        int result = removeDuplicates(nums);

        System.out.print("Modified Array: ");
        for (int i = 0; i < result; i++) {
            System.out.print(nums[i] + " ");
        }

        for (int i = result; i < nums.length; i++) {
            System.out.print(" _ ");
        }


        System.out.println();

        
        System.out.println("Number of Unique Elements: " + result);
    }

}

